"""Simulation of an object affected by Earth's gravity using differential equations and numerical
   integration. Supplementary material for V2SIM Maths."""

import math
from typing import Tuple
import sys

import pygame

if len(sys.argv) < 2:
    sys.exit(0)

size = int(sys.argv[1])

SCALE = 6e4                       # larger values zoom out

EARTH_RADIUS = 6e5                # m
EARTH_MASS = 5.97e24              # kg
GRAVITATIONAL_CONSTANT = 6.67e-11 # N m2 / kg2

class Simulation():
    """Class to contain all simulation variables and constants."""

    def __init__(self, screen, position, velocity, h):
        self.canvas = screen
        self.position = position
        self.velocity = velocity
        self.running = True
        self.h = h

        # Setup PyGame
        pygame.init()
        self.font = pygame.font.Font('Retro Gaming.ttf', 16)
        self.screen = pygame.display.set_mode(
                (self.canvas[0], self.canvas[1]),
                pygame.constants.RESIZABLE
        )


    def earth_distance(self) -> float:
        """Calculate spaceship distance to earth.

        Returns:
            Distance as float value in m."""
        return math.sqrt( self.position[0]**2 + self.position[1]**2 )


    def crashed(self) -> bool:
        """Collision detection for spaceship.

        Returns:
            True if crashed, False if not."""
        return self.earth_distance() <= EARTH_RADIUS


    def acceleration(self) -> Tuple[float, float]:
        """Calculate acceleration from gravity for current coordinates.

        Returns:
            Acceleration in x and y direction respectively.
        """

        result = -GRAVITATIONAL_CONSTANT * EARTH_MASS / self.earth_distance() ** 3
        return (result * self.position[0], result * self.position[1])

    def increment_x(self, dx: float = None):
        """Increment spaceship x by a given value, or spaceship dx if none provided.

        Args:
            dx: Signed change in x.
        """

        if not dx:
            dx = self.velocity[0]
        self.position[0] += self.h * dx


    def increment_y(self, dy: float = None):
        """Increment spaceship y by a given value, or spaceship dy if none provided.

        Args:
            dy: Signed change in y.
        """
        if not dy:
            dy = self.velocity[1]
        self.position[1] += self.h * dy


    def increment_dx(self, d2x: float = None):
        """Increment spaceship dx by a given value.

        Args:
            d2x: Signed change in dx.
        """
        if not d2x:
            d2x = self.acceleration()[0]
        self.velocity[0] += self.h * d2x


    def increment_dy(self, d2y: float = None):
        """Increment spaceship dy by a given value.

        Args:
            d2y: Signed change in dy.
        """
        if not d2y:
            d2y = self.acceleration()[1]
        self.velocity[1] += self.h * d2y


    def fold_universe(self):
        """Prevent the spaceship from going offscreen by wrapping the universe as a torus (similar
           to Asteroids)."""
        visible = self.canvas[0]*SCALE
        offset = visible/2
        self.position[0] = (self.position[0] + offset) % visible - offset

        visible = self.canvas[1]*SCALE
        offset = visible/2
        self.position[1] = (self.position[1] + offset) % visible - offset


    def handle_keys(self):
        """Key handling part of the main pygame loop."""
        keys = pygame.key.get_pressed()

        if keys[pygame.constants.K_ESCAPE] or keys[pygame.constants.K_q]:
            self.velocity[0] = self.velocity[1] = 0
            self.h = 20      # Speedup for crash
        if keys[pygame.constants.K_LEFT] or keys[pygame.constants.K_h]:
            self.increment_dx(-100)
        if keys[pygame.constants.K_DOWN] or keys[pygame.constants.K_j]:
            self.increment_dy(100)
        if keys[pygame.constants.K_UP] or keys[pygame.constants.K_k]:
            self.increment_dy(-100)
        if keys[pygame.constants.K_RIGHT] or keys[pygame.constants.K_l]:
            self.increment_dx(100)
        if keys[pygame.constants.K_EQUALS]:
            self.h += 0.01
        if keys[pygame.constants.K_MINUS]:
            self.h -= 0.01


    def plot_text(self, text: str, x: int, y: int):
        """Helper function to show text at provided coordinates.

        Args:
            text: The text to display.
            x: Horizontal coordinate in screen pixels.
            y: Vertical coordinate in screen pixels.
        """
        self.screen.blit(
                self.font.render(text, True, (255, 255, 255), (0, 0, 0)),
                (x, y))


    def plot_help(self, keys: str, effect: str, y: int):
        """Helper function to show keybinding at specified vertical position (x is fixed).

        Args:
            keys: The key or keys associated with the effect.
            effect: The effect that pressing the key has on the simulation.
            y: Vertical coordinate in screen pixels.
        """
        self.plot_text(keys, 20, y)
        self.plot_text(effect, 240, y)


    def plot_value(self, label: str, value: str, x: int, derivative: bool = False):
        """Helper function to show a simulated value or derivative at a specified horizontal
           position (y is fixed).

        Args:
            label: The name of the mathematical value, such as x or y.
            value: The value associated with the label.
            x: Horizontal coordinate in screen pixels.
            derivative: A boolean indicating whether the label should be shown as a derivatie
                        (dx/dt) or plain (x).
        """
        if derivative:
            self.plot_text(f"d{label}", x, self.canvas[1]-40)
            self.plot_text("dt", x, self.canvas[1]-20)
            pygame.draw.line(
                    self.screen,
                    (255, 255 ,255),
                    (x, self.canvas[1]-18),
                    (x+20, self.canvas[1]-18)
            )
        else:
            self.plot_text(label, x+10, self.canvas[1]-30)

        self.plot_text(f"= {value}", x+30, self.canvas[1]-30)


    def plot_stuff(self):
        """Screen updating part of the main pygame loop."""
        # Partial screen clear
        s = pygame.Surface((self.canvas[0],self.canvas[1]), pygame.SRCALPHA) # Fill transparent
        s.fill((0,0,0,3))                              #  -> trail
        self.screen.blit(s, (0,0))

        # Draw planet and ship
        pygame.draw.circle(
                self.screen,
                (0,128,255),              # Lightblue
                (self.canvas[0]/2, self.canvas[1]/2),
                EARTH_RADIUS/SCALE
        )

        pygame.draw.circle(
                self.screen,
                (255,0,192),              # Purple
                (self.canvas[0]/2+self.position[0]/SCALE,
                 self.canvas[1]/2+self.position[1]/SCALE),
                4
        )

        # Clear bottom 30 pixels for printing values/differentials
        s = pygame.Surface((self.canvas[0],30), pygame.SRCALPHA)
        s.fill((0,0,0,255))
        self.screen.blit(s, (0,self.canvas[1]-30))

        # Plot values and differentials
        self.plot_value("x", f"{self.position[0]:.3f}", 20)
        self.plot_value("y", f"{self.position[1]:.3f}", 220)
        self.plot_value("x", f"{self.velocity[0]:.3f}", 420, derivative=True)
        self.plot_value("y", f"{self.velocity[1]:.3f}", 620, derivative=True)

        # Plot keybindings
        self.plot_help("h/j/k/l, [arrows]", "fire thrusters", 20)
        self.plot_help("q, [escape]", "full stop and crash", 40)
        self.plot_help("+/-", f"change h (h = {self.h:.3f})", 60)

        pygame.display.update()

sim = Simulation(
        screen = [size, size],   # pixels
        position = [15e6, 1e6],  # m
        velocity = [2e3, 4e3],   # m/s
        h = size/160,            # step size in frames, dependent on screen size
)


while sim.running:
    # Handle quit
    for event in pygame.event.get():
        if event.type == pygame.constants.QUIT:
            sim.running = False

    if sim.crashed():
        sim.running = False

    # Calculate acceleration from gravity (before updating position)
    ax, ay = sim.acceleration()

    # Apply current velocity to position
    sim.increment_x()
    sim.increment_y()

    # Handle keys for movement (updating velocity), changing h and ending the simulation.
    sim.handle_keys()

    # Apply pre-calculated acceleration to velocity
    sim.increment_dx(ax)
    sim.increment_dy(ay)

    # Fold universe into screen as torus
    sim.fold_universe()

    # Plot stuff
    sim.plot_stuff()
