"""Simulation of an object affected by Earth's gravity using differential equations and numerical
   integration. Supplementary material for V2SIM Maths."""

import math
from typing import Tuple
import sys

import pygame

if len(sys.argv) < 2:
    sys.exit(0)

size = max(800, int(sys.argv[1]))

if len(sys.argv) >= 3:
    seed = int(sys.argv[2])
else:
    seed = 12345

OWN_RNG = True
#colours = [(128,128,128)]
colours = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (0, 255, 255), (255, 0, 255), (255, 255, 0), (128, 128, 128)]


if OWN_RNG:
    import rng
else:
    import random

class Simulation():
    """Class to contain all simulation variables and constants."""

    def __init__(self, screen, seed):

        self.speed = 3
        self.colour = 0
        self.canvas = screen
        self.running = True


        # Setup PyGame
        pygame.init()
        self.font = pygame.font.Font('Retro Gaming.ttf', 16)
        self.screen = pygame.display.set_mode(
                (self.canvas[0], self.canvas[1] + 100),
                pygame.constants.RESIZABLE
        )

        self.reseed(seed)


    def reseed(self, seed):
        """Restart the simulation using a new seed."""
        self.seed = seed
        self.inside = 0
        self.outside = 0

        if OWN_RNG:
            self.rng = rng.MidSquareGen(seed)
        else:
            self.rng = random.seed(seed)

        s = pygame.Surface((self.canvas[0],self.canvas[1]), pygame.SRCALPHA)
        s.fill((0,0,0,255))
        self.screen.blit(s, (0,0))
        pygame.draw.circle(
                self.screen,
                (255,255,255),
                (self.canvas[0]/2, self.canvas[1]/2),
                self.canvas[0]/2
        )

    def handle_keys(self):
        """Key handling part of the main pygame loop."""
        keys = pygame.key.get_pressed()

        if keys[pygame.constants.K_ESCAPE] or keys[pygame.constants.K_q]:
            self.running = False
        if keys[pygame.constants.K_EQUALS]:
            self.speed += 0.5
        if keys[pygame.constants.K_MINUS]:
            self.speed -= 0.5
        if keys[pygame.constants.K_COMMA]:
            self.reseed(self.seed-1)
        if keys[pygame.constants.K_PERIOD]:
            self.reseed(self.seed+1)


    def plot_text(self, text: str, x: int, y: int):
        """Helper function to show text at provided coordinates.

        Args:
            text: The text to display.
            x: Horizontal coordinate in screen pixels.
            y: Vertical coordinate in screen pixels.
        """
        self.screen.blit(
                self.font.render(text, True, (255, 255, 255), (0, 0, 0)),
                (x, y))


    def plot_help(self, keys: str, effect: str, y: int):
        """Helper function to show keybinding at specified vertical position (x is fixed).

        Args:
            keys: The key or keys associated with the effect.
            effect: The effect that pressing the key has on the simulation.
            y: Vertical coordinate in screen pixels.
        """
        self.plot_text(keys, 20, y)
        self.plot_text(effect, 240, y)


    def plot_stuff(self):
        """Screen updating part of the main pygame loop."""

        s = pygame.Surface((self.canvas[0],100), pygame.SRCALPHA)
        s.fill((0,0,0))
        self.screen.blit(s, (0,self.canvas[1]))

        if OWN_RNG:
            x = self.rng.next()
            y = self.rng.next()
        else:
            x = random.random()
            y = random.random()

        if (x * 2 - 1)**2 + (y * 2 - 1)**2 < 1:
            self.inside += 1
        else:
            self.outside += 1

        self.colour = (self.colour + 1) % len(colours)

        # Draw planet and ship
        pygame.draw.circle(
                self.screen,
                colours[self.colour],
                (x*size, y*size),
                10
        )

        total = self.inside + self.outside

        self.plot_text(f"Inside: {self.inside}", 20, size + 10)
        self.plot_text(f"Outside: {self.outside}", 140, size + 10)
        self.plot_text(f"Ratio: {(self.inside/total):.2f}", 260, size + 10)
        self.plot_text(f"Pi: {(4*self.inside/total):.3f}", 380, size + 10)
        self.plot_text(f"RNG: {"MS" if OWN_RNG else "Python"}", 500, size + 10)
        self.plot_text(f"Seed: {self.seed}", 620, size + 10)

        pygame.display.update()

sim = Simulation(
        screen = [size, size],   # pixels
        seed = seed,
)

clock = pygame.time.Clock()

while sim.running:
    # Handle quit
    for event in pygame.event.get():
        if event.type == pygame.constants.QUIT:
            sim.running = False

    # Handle keys for movement (updating velocity), changing h and ending the simulation.
    sim.handle_keys()

    # Plot stuff
    sim.plot_stuff()

    clock.tick(sim.speed)
