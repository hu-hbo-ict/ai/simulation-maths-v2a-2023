# Som in Cumulatieve Geometrische Verdeling
De som die in de cumulatieve versie van de geometrische verdeling ontstaat is te versimpelen tot een enkele machtsverheffing door gebruik te maken van het volgende lemma. Deze werkt voor iedere $x \in \mathbb{R}$ dus ook in $[0,1] \subset \mathbb{R}$. Met dank aan Levi voor de suggestie.

$$\sum_{k=1}^{n} \big[ x (1-x)^{k-1} \big] = 1 - (1-x)^n \quad\text{voor}\quad n \in \mathbb{N}_0,\ x \in \mathbb{R}$$

## Bewijs
Het lemma kan worden bewezen met inductie over $n$.

#### Base case:

In het basisgeval vullen we voor $n$ de waarde $0$ in, en tonen aan dat beide zijden gelijk zijn.

$$\sum_{k=1}^{0} \big[ x (1-x)^{k-1} \big] = 1 - (1-x)^0$$

De linkerkant is een lege som (want de eindwaarde $0 < 1$, de beginwaarde), dus dat is $0$. De rechtkant is $1 - \varphi^0 = 1 - 1 = 0$. Beide kanten zijn gelijk.

#### Inductiestap:

Voor de inductiestap bewijzen we het lemma voor de waarde $n+1$, waarbij we gebruik maken van het lemma voor de waarde $n$.

$$\begin{align}\sum_{k=1}^{n+1} \big[ x (1-x)^{k-1} \big] + (1-x)^{n+1}
& = \sum_{k=1}^{n} \big[ x (1-x)^{k-1} \big] + x(1-x)^n + (1-x)^{n+1} & \text{Laatste term buiten de som halen, $k-1 = (n+1)-1 = n$} \\
& = \sum_{k=1}^{n} \big[ x (1-x)^{k-1} \big] + x(1-x)^n + (1-x)(1-x)^{n} & \text{Enkele term uit machtsverheffing halen} \\
& = \sum_{k=1}^{n} \big[ x (1-x)^{k-1} \big] + (x+(1-x))(1-x)^n & \text{Distributie over $(1-x)^n$}\\
& = \sum_{k=1}^{n} \big[ x (1-x)^{k-1} \big] + (1-x)^n & \text{$x+1-x = 1$}\\
& = 1 & \text{Per inductie} \\
\end{align}$$
